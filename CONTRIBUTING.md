# Development notes on the dummy modules

## Type Hinting

All packages only need to run from PC, so all packages are available.

This means that `typing` can also be used to increase type hinting verbosity, which is very useful. 

## `biorobotics` package

Since the BioRobotics package is meant to be built entirely on top of the micropython packages, it was decided to use it directly in these dummy modules. That means that inside the `biorobotics` package **no dummy features** can be added.  
This is also not necessary, since complete functionality can still be achieved by the other dummy modules alone.

## Class and Function Exposing

Inside each `__init__.py` is defined which features should be exposed in the package. Everything that is not listed there is not readily available. This init file is executed when the module is being imported.

## Pip packaging

To make installing easier the modules have been wrapped as a pip package. This defined through `setup.py`.

It would be sensible if the biorobotics-package repo were a pip package too. However, then users would have to install two packages. There seems to be no way to embed a package in a subdirectory.
