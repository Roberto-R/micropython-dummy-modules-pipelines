from typing import Optional
import socket
import threading
import atexit
import select


class SerialServer(threading.Thread):
    """socket based server to transmit data, faking a serial connection

    Class will handle communications from its own thread.

    Call .start() to activate the server and spawn the necessary threads
    """

    HOST = '127.0.0.1'  # Localhost
    PORT = 52184  # Random port

    def __init__(self, *args, **kwargs):
        """Constructor"""

        self.socket = None  # type: Optional[socket.socket]
        self._open = False

        # Listening client
        self.client = None  # type: Optional[socket.socket]

        atexit.register(self.close)

        super().__init__(*args, daemon=True, name='Serial server thread',
                         **kwargs)
        # In daemon _mode the thread does not keep the script alive

    def __del__(self):
        """Destructor"""
        self.close()  # pragma: no cover

    def run(self):
        """Actually open socket and start listening

        This method is run in a separate thread.
        """

        # Initialize TCP/IP socket
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Set option to allow instant socket reuse after shutdown
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.socket.bind((self.HOST, self.PORT))

        self.socket.listen()

        self._open = True

        # Accept connections
        while self.is_open():

            ready, _, _ = select.select([self.socket], [], [], 0.1)

            if not ready:
                continue

            try:
                (client, address) = self.socket.accept()
            except OSError:
                # With unfortunate timing the socket could already be closed
                continue

            if self.client is not None:
                client.close()
                continue  # Already have a client, do not accept a new

            self.client = client

    def close(self):
        """Stop server"""
        if self.client is not None:
            self.client.close()
            self.client = None

        self._open = False  # Let while loop run out

        if self.socket is not None:
            self.socket.close()

    def is_open(self):
        """Check if server is running"""
        return self._open

    def send(self, data: bytes):
        """Send bytes to a the client

        If there is no client, nothing will happen.
        This method is still synchronous!
        """

        if not self.is_open():
            return  # Can't be sending anything

        if self.client is None:
            return  # Nobody to send to

        try:
            self.client.send(data)
        except (ConnectionResetError, ConnectionAbortedError):
            # Connection by closed by client, clear it and carry on
            self.client.close()
            self.client = None
