"""dummy uart3br package

This package is normally a custom serial port. In this dummy version it is
turned into a TCP socket, to allow communication with e.g. the uScope
instead, simulating a serial connection.
"""

from ._uart3br import send
