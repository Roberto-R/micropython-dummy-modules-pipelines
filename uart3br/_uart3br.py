import time

from ._serial_server import SerialServer


# Global instance of SerialServer
# No sockets are created on object construction, to limit overhead when
# socket wouldn't even be used.
server = SerialServer()


def send(chunks: bytes):
    """Send serial data

    Dummy version simply does nothing
    """

    if not server.is_open():
        server.start()

    server.send(chunks)
