import unittest
from unittest import mock
import time

from biorobotics import Ticker, tic, toc


class TestTicker(unittest.TestCase):

    def test_ticker(self):
        """Test basic ticker function"""

        callback = mock.MagicMock()

        t1 = Ticker(0, freq=100.0, callback=callback)

        self.assertGreater(t1.timer_number, 0)

        time.sleep(0.10)  # Give ticker some time

        callback.assert_not_called()

        t1.start()  # Now actually start the ticker

        time.sleep(0.20)  # Let it make some calls

        count = callback.call_count
        # Accept around 20 calls, not exact because time.sleep() and the
        # threading timer are both not accurate
        self.assertTrue(10 <= count <= 30, 'Function was called {} '
                                           'times'.format(count))

    @mock.patch('builtins.print')
    def test_tic_toc(self, print_mock: mock.MagicMock):
        """Use mock to prevent actual output"""

        tic()
        print_mock.assert_not_called()

        time.sleep(0.20)  # Have some time to be measured

        micros = toc()

        self.assertTrue(0.18e6 <= micros < 0.23e6)
        print_mock.assert_called_once()

    def test_use_hardware(self):
        """Test ticker creation with hardware number

        Also trigger the garbage collection
        """
        callback = mock.MagicMock()
        t1 = Ticker(5, use_hardware_id=True, callback=callback, freq=100.0,
                    enable_gc=True)
        t1.start()

        time.sleep(0.10)  # Let at least one call pass

        t1.stop()

        self.assertEqual(5, t1.timer_number)
        self.assertGreater(callback.call_count, 0)
        self.assertFalse(t1.is_started)

    def test_invalid_number(self):
        """Test ticker creation with hardware number"""
        callback = mock.MagicMock
        with self.assertRaises(ValueError):
            Ticker(20, callback=callback, freq=1.0)


if __name__ == '__main__':
    unittest.main()
