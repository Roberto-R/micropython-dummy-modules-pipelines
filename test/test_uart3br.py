import unittest
import sys
import time


class TestUART3BR(unittest.TestCase):
    """Test the custom uart3 module

    The SerialServer itself is tested in test_serial_server.py.
    """

    def tearDown(self):
        """Clean after each test"""

        # Reset the module
        if 'uart3br' in sys.modules:
            del sys.modules['uart3br']

    def test_sockets(self):
        """Make sure are only opened when needed"""

        import uart3br

        self.assertFalse(uart3br._uart3br.server.is_open())

        uart3br.send(b'\x00')

        time.sleep(0.2)

        self.assertTrue(uart3br._uart3br.server.is_open())


if __name__ == '__main__':
    unittest.main()
