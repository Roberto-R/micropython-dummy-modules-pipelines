import unittest

from pyb import Pin, Timer
from biorobotics import PWM


class TestTimerPWM(unittest.TestCase):

    def test_timer(self):
        """Test the 'classic' method of creating a PWM"""

        tim = Timer(1, freq=1000.0)
        pwm = tim.channel(1, Timer.PWM, pin=Pin('D5'))
        pwm.pulse_width_percent(15.0)
        self.assertEqual(15.0, pwm.test_get_value())

        Timer(1).channel(1, Timer.PWM).pulse_width_percent(35.0)

        self.assertEqual(35.0, pwm.test_get_value())


class TestPWM(unittest.TestCase):

    def test_pwm(self):
        """Test the fancy PWM class"""

        pwm = PWM('D5', 1000.0)
        pwm.write(0.45)
        self.assertEqual(45.0, pwm.channel.test_get_value())

        PWM('D5').write(0.62)
        self.assertEqual(62.0, pwm.channel.test_get_value())

        pin = Pin('D5')
        PWM(pin).write(0.89)
        self.assertEqual(89.0, pwm.channel.test_get_value())

    def test_invalid_pin_timer(self):
        """Test encoder creation from a Pin without Timer"""

        with self.assertRaises(ValueError) as context:
            PWM('D7')

        self.assertIn('does not have a registered PWM timer',
                      str(context.exception))

    def test_invalid_pin_type(self):
        """Test encoder creation from an incorrect argument"""

        with self.assertRaises(ValueError) as context:
            PWM(10)

        self.assertIn('Cannot convert',
                      str(context.exception))

    def test_write_cap(self):
        """Test the PWM capper"""

        pwm = PWM('D5')
        pwm.write(-0.5)
        self.assertEqual(0.0, pwm.channel.test_get_value())
        pwm.write(1.5)
        self.assertEqual(100.0, pwm.channel.test_get_value())


if __name__ == '__main__':
    unittest.main()
