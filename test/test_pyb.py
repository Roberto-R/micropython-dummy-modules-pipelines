import unittest
from unittest import mock

from pyb import Switch, Pin, LED


class TestSwitch(unittest.TestCase):

    def test_switch(self):

        sw = Switch()
        self.assertEqual(False, sw.value())

        sw.test_set_value(True)
        self.assertEqual(True, sw.value())

        Switch().test_set_value(False)  # Value is not dependent on instance
        self.assertEqual(False, sw.value())

    def test_callback(self):

        callback = mock.MagicMock()

        sw = Switch()
        sw.callback(callback)
        callback.assert_not_called()  # Must not be triggered yet

        sw.test_set_value(True)  # Press
        sw.test_set_value(False)
        callback.assert_called_once()

        sw.callback(None)

        sw.test_set_value(True)  # Press
        sw.test_set_value(False)
        callback.assert_called_once()  # Not called again

    def test_push(self):

        callback = mock.MagicMock()

        sw = Switch()
        sw.callback(callback)

        sw.test_push()

        callback.assert_called_once()
        self.assertFalse(sw.value())


class TestPin(unittest.TestCase):

    def test_read(self):
        pin = Pin('D9', mode=Pin.IN)
        self.assertEqual(0, pin.value())
        Pin('D9').test_set_value(1)
        self.assertEqual(1, pin.value())

    def test_write(self):
        pin = Pin('D8', mode=Pin.OUT)
        Pin('D8').value(1)
        self.assertEqual(1, pin.value())

    def test_write_toggle(self):
        pin = Pin('D7', mode=Pin.OUT)
        self.assertEqual(0, pin.value())
        pin.on()
        self.assertEqual(1, pin.value())
        pin.off()
        self.assertEqual(0, pin.value())

    def test_mode(self):
        pin = Pin('D6')
        pin.mode(Pin.OUT)
        self.assertEqual(Pin.OUT, pin.mode())

    def test_pull(self):
        pin = Pin('D6')
        pin.pull(Pin.PULL_UP)
        self.assertEqual(Pin.PULL_UP, pin.pull())


class TestLED(unittest.TestCase):

    def test_led(self):
        led = LED(1)
        led.on()
        self.assertTrue(led.test_is_on())
        led.off()
        self.assertFalse(led.test_is_on())
        LED(1).toggle()
        self.assertTrue(led.test_is_on())

    def test_intensity(self):
        led = LED(1)
        led.intensity(255)
        val = led.intensity()
        self.assertEqual(val, 255)


if __name__ == '__main__':
    unittest.main()
