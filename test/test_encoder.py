import unittest

from pyb import Pin, Timer
from biorobotics import Encoder


class TestEncoder(unittest.TestCase):

    def setUp(self) -> None:
        Timer._values = {}  # Reset any old values

    def test_timer(self):
        """Test classic encoder first"""

        tim = Timer(3, prescaler=0, period=0xFFFF)

        tim.channel(2, Timer.ENC_AB, pin=Pin('D12'))
        tim.channel(1, Timer.ENC_AB, pin=Pin('D11'))

        self.assertEqual(0, tim.counter())

    def test_encoder(self):
        """Test fancy class"""

        encoder = Encoder('D12', 'D11')
        self.assertEqual(0, encoder.counter())

        Encoder('D12', 'D11').set_counter(1234)
        self.assertEqual(1234, encoder.counter())

    def test_incorrect_pins(self):
        """Test encoder creation on pins without a timer"""

        with self.assertRaises(ValueError) as context:
            Encoder('D12', 'D7')

        self.assertIn('not connected to a hardware timer',
                      str(context.exception))

    def test_incorrect_timers(self):
        """Test encoder creation on pins without different timers"""

        with self.assertRaises(ValueError) as context:
            Encoder('D12', 'D10')

        self.assertIn('are not connected to the same hardware timer',
                      str(context.exception))

    def test_unwrapper(self):
        """Test the encoder unwrapper"""

        encoder = Encoder('D12', 'D11')

        encoder.timer.test_set_value(65530)
        # It's valid for the timer to have a non-zero count at the start
        self.assertEqual(65530, encoder.counter())

        encoder.timer.test_set_value(65534)
        self.assertEqual(65534, encoder.counter())
        encoder.timer.test_set_value(65535)  # Highest value
        self.assertEqual(65535, encoder.counter())
        encoder.timer.test_set_value(0)  # First new value of a complete wrap
        self.assertEqual(65536, encoder.counter())
        encoder.timer.test_set_value(1)  # First new value of a complete wrap
        self.assertEqual(65537, encoder.counter())

    def test_spinning(self):
        """Count up and down and confirm the outcome"""

        encoder = Encoder('D12', 'D11')

        for k in range(2):  # Two loops
            for c in range(0, 70000, 10000):
                encoder.timer.test_set_value(c)  # Write increasing count
                encoder.counter()  # Call counter, otherwise unwrap won't work

        self.assertEqual(60000 + 65536, encoder.counter())

        # And turn it backwards again:
        for k in range(2):  # Two loops (count should now be back at 0)
            for c in range(60000, -10000, -10000):  # range() is exclusive
                encoder.timer.test_set_value(c)
                encoder.counter()

        self.assertEqual(0, encoder.counter())

        # And turn it backwards further:
        for c in range(60000, -10000, -10000):  # range() is exclusive
            encoder.timer.test_set_value(c)
            encoder.counter()

        self.assertEqual(-65536, encoder.counter())  # Net result: one
        # negative rotation

    def test_set_count(self):
        """Set the set_counter() method"""

        encoder = Encoder('D12', 'D11')
        self.assertEqual(0, encoder.counter())

        encoder.set_counter(1024)
        self.assertEqual(1024, encoder.counter())

        encoder.set_counter(123456)  # Should be wrapping
        self.assertEqual(123456, encoder.counter())

        encoder.set_counter(-123456)  # Should be wrapping
        self.assertEqual(-123456, encoder.counter())


if __name__ == '__main__':
    unittest.main()
