import unittest
import threading
import socket
import struct
import time
import atexit
import select

from uart3br._serial_server import SerialServer


class SerialClient(threading.Thread):
    """Class to receive socket data

    Used to test the socket server. And provides an example to real
    application clients.
    """

    def __init__(self, host: str, port: int, *args, **kwargs):
        """Constructor"""

        self.host = host
        self.port = port

        self._open = False

        # All the received data:
        self.data_list = bytearray()  # type: bytearray

        atexit.register(self.close)

        super().__init__(*args, daemon=True, name='Serial client thread',
                         **kwargs)
        # In daemon _mode the thread does not keep the script alive

    def __del__(self):
        """Destructor"""
        self.close()

    def run(self):
        """Actually open socket and start listening

        This method is run in a separate thread.
        """

        self._open = True

        # Initialize TCP/IP socket
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

            s.connect((self.host, self.port))

            while self.is_open():

                ready, _, _ = select.select([s], [], [], 0.1)

                if not ready:
                    continue

                try:
                    data = s.recv(1024)
                except ConnectionResetError:
                    # Problems with a second connection
                    continue

                if data:
                    self.data_list.extend(data)

    def close(self):
        """Stop server"""
        if self._open:
            self._open = False  # Let while loop run out
            self.join()  # Wait for listening thread to converge

    def is_open(self):
        """Check if server is running"""
        return self._open


class TestSerialServer(unittest.TestCase):

    def setUp(self):
        """Before each test"""
        self.server = SerialServer()
        self.server.start()

        time.sleep(0.3)  # Give server time to start up

    def tearDown(self):
        """Before each test"""
        self.server.close()
        self.server = None  # Remove server again

    def test_send(self):
        """Test how the server sends data to a client"""

        client = SerialClient(self.server.HOST, self.server.PORT)
        client.start()  # Start client

        time.sleep(0.3)  # Give client time to connect

        values = [314, 420, 123]

        for value in values:
            data = struct.pack('I', value)
            self.server.send(data)

        time.sleep(0.3)  # Give a moment to let the messages cross over

        client.close()

        data_received = client.data_list

        values_received = list(struct.unpack('III', data_received))

        self.assertEqual(values, values_received)

    def test_no_clients(self):
        """Test how the server sends without a client"""

        try:

            values = [314, 420, 123]

            for value in values:
                data = struct.pack('I', value)
                self.server.send(data)

            time.sleep(0.3)  # Give a moment to let the messages cross over

        except:  # noqa 722
            self.fail('Error thrown by serial server!')

        # Let destructor close connection

    def test_client_disconnect(self):
        """Test how the server sends data to a client"""

        client = SerialClient(self.server.HOST, self.server.PORT)
        client.start()  # Start client

        time.sleep(0.3)  # Give client time to connect

        values = [314, 420, 123]

        for value in values:
            data = struct.pack('I', value)
            self.server.send(data)

        time.sleep(0.3)  # Give a moment to let the messages cross over

        # Get rid of the client
        client.close()

        # Send data again
        for value in values:
            data = struct.pack('I', value)
            self.server.send(data)

        time.sleep(0.3)  # Give a moment to let the messages cross over

        self.assertIsNone(self.server.client)  # Make sure client was
        # unregistered

        self.server.close()

    def test_two_client(self):
        """Test how the server reacts to multiple clients"""

        client = SerialClient(self.server.HOST, self.server.PORT)
        client.start()  # Start client

        time.sleep(0.3)

        client2 = SerialClient(self.server.HOST, self.server.PORT)
        client2.start()  # Start another client

        time.sleep(0.1)  # Give client time to connect

        self.server.send(b'\x10\x10\x10')

        time.sleep(0.1)  # Give time to receive

        self.assertGreaterEqual(len(client.data_list), 1)
        self.assertEqual(0, len(client2.data_list))


class TestPassiveSerialServer(unittest.TestCase):

    def test_empty(self):
        """Test a server """

        try:
            # Let server instance run out of scope without doing anything
            server = SerialServer()
            del server
        except:  # noqa 722
            self.fail('Error thrown by serial server!')


if __name__ == '__main__':
    unittest.main()
