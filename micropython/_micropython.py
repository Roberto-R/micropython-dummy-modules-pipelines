"""
micropython package, dummy version
"""

from typing import Callable, Any


def alloc_emergency_exception_buf(buffer: int):
    """Set exception buffer size, useful to interrupt exceptions"""
    return


def schedule(func: Callable, arg: Any):
    """Schedule a function call"""

    func(arg)  # In dummy version we just call it right away
