"""Dummy memory array class"""


class Mem:

    def __getitem__(self, item):
        """Overload `[...]` getter"""
        return 0

    def __setitem__(self, key, value):
        """Overload `[...]` setter"""
        return None
