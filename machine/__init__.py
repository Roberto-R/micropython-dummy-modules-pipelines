"""Dummy machine package"""

from pyb import Pin, Timer
from ._machine import mem8, mem32, ADC

from pyb import _system_check  # Verify platform - Will throw an import
# error if only this package was uploaded
