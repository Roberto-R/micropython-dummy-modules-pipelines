"""Dummy pyb package"""

from ._timer import Timer
from ._pyb import LED, Switch, elapsed_millis, millis, delay, micros, udelay
from ._pin import Pin
from ._uart import UART

from . import _system_check  # Verify platform
