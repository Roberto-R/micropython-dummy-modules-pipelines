import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="micropython-dummy-modules-BE",  # Add username to prevent conflicts
    version="0.0.1",
    author="Robert Roos",
    author_email="robert.soor@gmail.com",
    description="Modules to mirror micropython modules, to do local testing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/ctw-bw/micropython-dummy-modules",
    packages=["biorobotics", "machine", "micropython", "pyb", "stm",
              "uart3br", "utime"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
