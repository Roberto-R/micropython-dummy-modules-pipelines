# Installing the modules

Select one of the following methods to make the modules available.

## Pip global install

For Windows:

 1. Clone or download the dummy modules repository at a recognizable place, like `C:\Program Files\micropython\dummy_modules`
 2. Open a command prompt and run:
    ```
    pip install "C:\Program Files\micropython\dummy_modules"
    ```
    Change the path appropriately.
 3. The modules should now be available everywhere.

For Linux/Mac the steps are the same, only the paths look different.

## Use a virtual environment

You can create a virtual environment, which acts as a stand-alone Python installation, around the dummy modules. If you use Python often, this is recommended.  
For Windows:

 1. Clone or download the dummy modules repository at a recognizable place, like `C:\Program Files\micropython\dummy_modules`
 2. Open a CMD or PowerShell terminal in the dummy_modules directory
 3. Run: `python -m venv venv_micropython` to use the 'venv' module to create an environment named 'venv_micropython'
 4. To activate the venv, run `"C:\Program Files\micropython\dummy_modules\venv_micropython\Scrpts\activate.bat"` from that terminal. (Use `Activate.ps1` for PowerShell instead.) To exit the venv again, you could run `deactivate`.
    *  Note the "(venv_micropython)" prefix in your terminal.
 5. To install the modules in this venv, run `pip install "C:\Program Files\micropython\dummy_modules"` (while the venv is activated)
    *  To develop the dummy modules or to make updating easier, use `... install -e ...` ('e' for editable). This will create a link to the original source instead of copying it.
 6. The modules are now available in the venv. (But not in the regular Python install.)
 7. To select this venv in VS Code, open your project folder, hit [F1] and type or select "Python: Select interpreter". Click "Select..." and click "Find...". Select `C:\Program Files\micropython\dummy_modules\venv_micropython\Scrpts\python.exe`

You can recognize an activated environment by the '(venv_micropython)' flag at the start of the terminal.

## Micropython for PC

These modules are frozen in micropython Unix and Windows ports in https://bitbucket.org/ctw-bw/micropython/wiki/Home. So running that version of micropython will already have the dummy modules included.  
See the wiki of the micropython repository for more instructions on micropython for PC.

## Run `sys.path.append`

Put the dummy modules alongside (**not** inside) your project. Add this to the top of your main file:
```
import sys
sys.path.append('../dummy_modules')
```

This will expand your python path at runtime.

This method is *not recommended*, as it requires modifications to your program to suit testing on PC. Additionally, VS Code can probably not resolve this runtime extension, meaning that it cannot verify any syntax around those modules.
