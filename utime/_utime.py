"""
utime dummy package
"""

import time


class Timing:
    """Put start time in class to avoid using a global variable"""
    start_time_s = time.time()  # Initialize the start time at boot


def ticks_us() -> int:
    time_s = time.time() - Timing.start_time_s  # ticks should count from boot
    return int(time_s * 1e6)  # time() returns timestamp in seconds


def ticks_ms() -> int:
    time_s = time.time() - Timing.start_time_s  # ticks should count from boot
    return int(time_s * 1e3)  # time() returns timestamp in seconds


def test_set_ticks(time_s: float):
    """Dummy method to set the current time, in seconds

    This new value would be returned by e.g. ticks_us() (in the correct units).
    """
    Timing.start_time_s = time.time() - time_s
