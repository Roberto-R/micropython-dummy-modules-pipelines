"""utime package"""

from ._utime import ticks_ms, ticks_us, test_set_ticks
